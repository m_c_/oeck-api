requirements:
1) python2 (tested 2.7)
2) mysql (tested with 5.1 and)
3) sqlalchemy (tested 1.0.9) for db
4) pymysql (tested 0.6.7) for MySQL
5) ldap3 (tested 0.9.9.3) for OpenLDAP server
6) pika (tested 0.10.0) for RabbitMQ
7) flask (tested 0.10.1) for web.py (optional)

change settings.py before starting
then run 'python first.py' for initializing DB

run 'python main.py' for starting XmlRpc server 

web.py and 'templates' folder are not parts of api,
after running 'python web.py', web-interface will be accessible at 127.0.0.1:5000 