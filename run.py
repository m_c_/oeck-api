# -*- coding: utf-8 -*-
'''
for manual testing purposes
'''

'''
from traceback import format_exc
format_exc()

from api import WebService

ws = WebService()

ws.addUser('testuser', 'password')

ws.addRegion('testregion', 'test.server.com', 'friendlyName')
ws.addRegionSpecificHost('testregion', 'test.host', '127.0.0.254')

ws.addRegion('testRegion1', 'test1.server.com', 'friendlyName1')
ws.addRegionSpecificHost('testRegion1', 'test1.host', '127.0.0.253')

ws.addRegion('testRegion2', 'test2.server.com', 'friendlyName2')
ws.addRegionSpecificHost('testRegion2', 'test2.host', '127.0.0.252')

ws.addChannel('testchannel1')
ws.addChannelRecord('testchannel1', 'test3.host', True)

ws.addChannel('testchannel2')
ws.addChannelRecord('testchannel2', 'test4.host', False)

ws.addChannel('testchannel3')
ws.addChannelRecord('testchannel3', 'test5.host', False)


ws.authorizeIP('127.0.0.1', 'testuser', 'testregion', {'testchannel1': 'testRegion1','testchannel2': 'testRegion2'}, 3600)
ws.authorizeIP('127.0.0.2', 'testuser', 'testregion', {'testchannel3': 'testRegion1'}, 3600)
ws.authorizeIP('127.0.0.3', 'testuser', 'testregion', {'testchannel3': 'testRegion1'}, 3600)
ws.authorizeIP('127.0.0.4', 'testuser', 'testregion', {'testchannel3': 'testRegion1'}, 3600)

ws.deauthorizeIP('127.0.0.2')

#print(ws.getUserObjects())
#print(ws.getRegionObjects())
#print(ws.getChannelObjects())

#ws.cleanIPs()
'''