# -*- coding: utf-8 -*-
from sqlalchemy import Column, Integer, String, Table, ForeignKey, Boolean, UniqueConstraint
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import validates, relationship
from hashlib import md5

Base = declarative_base()

class ValidationError(Exception):
    pass

def initDb(session):
    '''
    Function for creating tables
    @param session: SQLAlchemy session 
    '''
    Base.metadata.create_all(session.get_bind())

def validateLength(val, title, minLen, maxLen):
    '''
    Validates length of string
    @param val: str
    @param title: str
    @params minLen: int
    @param maxLen: int
    '''
    if len(val) < minLen or len(val) > maxLen:
        raise ValidationError('Length of {0} must be between {1} and {2} symbols'.format(title, minLen, maxLen))

def validateHostName(host, title):
    '''
    Validates FQDN
    thanks to Tim Pietzcker from StackOverflow
    http://stackoverflow.com/users/20670/tim-pietzcker
    @param host: str
    @param title: str 
    '''
    import re

    validateLength(host, 'server', 4, 255)
    if host[-1] == '.':
        host = host[:-1]
    if not '.' in host:
        raise ValidationError('Wrong {0}: \'{1}\''.format(title, host)) 

    allowed = re.compile('(?!-)[A-Z\d-]{1,63}(?<!-)$', re.IGNORECASE)
    if not all(x == '*' or allowed.match(x) for x in host.split('.')):
        raise ValidationError('Wrong {0}: \'{1}\''.format(title, host))

def validateIp(ip):
    '''
    Validate IPv4 addresses
    @param ip: str
    '''
    import re

    validateLength(ip, 'IPv4', 7, 15)
    
    allowed = re.compile('^[0-9]{1,3}$') 
    for x in ip.split('.'):
        if not allowed.match(x):
            raise ValidationError('Wrong IPv4 format: \'{0}\''.format(ip))


users_to_services = Table(
    'users_to_services',
    Base.metadata,
    Column('user_id', Integer, ForeignKey('users.id'), nullable=False),
    Column('service_id', Integer, ForeignKey('services.id'), nullable=False),
    mysql_engine='InnoDB',
    mysql_charset='utf8'
)


class Service(Base):
    '''
    Class represents service
    '''
    __tablename__ = 'services'
    __table_args__ = {'mysql_engine': 'InnoDB', 'mysql_charset': 'utf8'}

    id = Column(Integer, primary_key=True)
    name = Column(String(100, collation='utf8_bin'), nullable=False, unique=True)

    @validates('name')
    def validate_name(self, attr, name):
        '''
        Validate name
        @param attr: name of attr 
        @param name: value of attr
        @return: str
        '''
        validateLength(name, 'service name', 3, 100)
        return name


class User(Base):
    '''
    Class represents user
    '''
    __tablename__ = 'users'
    __table_args__ = {'mysql_engine': 'InnoDB', 'mysql_charset': 'utf8'}
    
    id = Column(Integer, primary_key=True)
    name = Column(String(100, collation='utf8_bin'), nullable=False, unique=True)
    password = Column(String(255), nullable=False)
    maxIpCount = Column(Integer, nullable=False, default=2, server_default='2')
    ipCount = Column(Integer, nullable=False, default=2, server_default='2')
    services = relationship('Service',secondary=users_to_services)
    ips = relationship('Ip', backref='user', cascade='all,delete-orphan')

    @classmethod
    def securePassword(cls, password):
        '''
        Convert password to store in DB
        @param password: str
        @return str
        @todo hash?
        '''
        #validateLength(password, 'Password', 6, 25)
        return md5(password.encode('UTF-8')).hexdigest()

    @validates('name')
    def validateName(self, attr, name):
        '''
        Validate name
        @param attr: name of attr 
        @param name: value of attr
        @return: str
        '''
        validateLength(name, 'user name', 3, 100)
        return name

    '''
    @validates('password')
    def validatePassword(self, attr, passw):
        Validate password
        @param attr: name of attr 
        @param passw: value of attr
        @return: str
        validateLength(passw, 'password', 6, 255)
        return passw
    '''

    @validates('ipCount')
    def validateIpCount(self, attr, ipCount):
        '''
        Validate count of ips with help of ipCount
        @param attr: name of attr 
        @param ipCount: value of attr
        @return: int
        '''
        tmp = self.maxIpCount - len(self.ips)
        if tmp < 0:
            raise ValidationError('Allowed number of IP\'s for the user is exceeded')
        return tmp

    def asDict(self):
        '''
        Return dict with object's data 
        @return: dict
        '''
        return {
            'name': self.name,
            'ipCount': self.ipCount,
            'maxIpCount': self.maxIpCount,
            'ips': tuple(ip.asDict() for ip in self.ips)
        }


class Ip(Base):
    '''
    Class represents ip
    '''
    __tablename__ = 'ips'
    __table_args__ = {'mysql_engine': 'InnoDB', 'mysql_charset': 'utf8'}

    id = Column(Integer, primary_key=True)
    userId= Column(Integer, ForeignKey(User.id), nullable=False)
    ip = Column(String(15), nullable=False, unique=True)
    expire = Column(Integer, nullable=False)
    relations = relationship('Relation', cascade='all,delete-orphan')

    @validates('ip')
    def validateIp(self, attr, ip):
        '''
        Validate ip
        @param attr: name of attr 
        @param ip: value of attr
        @return: str
        '''
        validateIp(ip)
        return ip

    def asDict(self):
        '''
        Return dict with object's data
        @return: dict
        '''
        from datetime import datetime
        rels = {}
        for r in self.relations:
            rels.update(r.asDictForIp())
        return {
            'ip': self.ip,
            'expire': datetime.fromtimestamp(self.expire).isoformat(' '),
            'relations': rels
        }


class Region(Base):
    '''
    Class represents region
    '''
    __tablename__ = 'regions'
    __table_args__ = {'mysql_engine': 'InnoDB', 'mysql_charset': 'utf8'}

    id = Column(Integer, primary_key=True)
    name = Column(String(100, collation='utf8_bin'), nullable=False, unique=True)
    server = Column(String(255, collation='utf8_bin'), nullable=False)
    friendlyName = Column(String(100, collation='utf8_bin'), nullable=False)
    specHosts = relationship('SpecHost',cascade='all,delete-orphan')
    relations = relationship('Relation', backref='region', cascade='all,delete-orphan')
    
    @validates('name')
    def validateName(self, attr, name):
        '''
        Validate name
        @param attr: name of attr 
        @param name: value of attr
        @return: str
        '''
        validateLength(name, 'name', 3, 100)
        return name

    @validates('server')
    def validateServer(self, attr, server):
        '''
        Validate server
        @param attr: name of attr 
        @param server: value of attr
        @return: str
        '''
        validateHostName(server, 'server')
        return server

    @validates('friendlyName')
    def validateFriendlyName(self, attr, friendlyName):
        '''
        Validate friendlyName
        @param attr: name of attr 
        @param friendlyName: value of attr
        @return: str
        '''
        validateLength(friendlyName, 'friendlyName', 3, 100)
        return friendlyName

    def asDict(self):
        '''
        Return dict with object's data
        @return: dict
        '''
        return {
            'name': self.name,
            'server': self.server,
            'friendlyName': self.friendlyName,
            'specHosts': tuple(h.asDict() for h in self.specHosts)
        }

class SpecHost(Base):
    '''
    Class represents specific host for region
    '''
    __tablename__ = 'spec_hosts'
    __table_args__ = (
        UniqueConstraint('regionId', 'name', name='unique_host_in_region'),
        {
            'mysql_engine': 'InnoDB',
            'mysql_charset': 'utf8'
        }
    )

    id = Column(Integer, primary_key=True)
    regionId = Column(Integer, ForeignKey(Region.id), nullable=False)
    name = Column(String(255, collation='utf8_bin'), nullable=False)
    ip = Column(String(15), nullable=False)

    @validates('name')
    def validateName(self, attr, name):
        '''
        Validate name
        @param attr: name of attr 
        @param name: value of attr
        @return: str
        '''
        validateHostName(name, 'hostname')
        return name

    @validates('ip')
    def validateIp(self, attr, ip):
        '''
        Validate ip
        @param attr: name of attr 
        @param ip: value of attr
        @return: str
        '''
        validateIp(ip)
        return ip

    def asDict(self):
        '''
        Return dict with object's data
        @return: dict
        '''
        return {
            'name': self.name,
            'ip': self.ip
        }


class Channel(Base):
    '''
    Class represents channel
    '''
    __tablename__ = 'channels'
    __table_args__ = {'mysql_engine': 'InnoDB', 'mysql_charset': 'utf8'}

    id = Column(Integer, primary_key=True)
    name = Column(String(100, collation='utf8_bin'), nullable=False, unique=True)
    hosts = relationship('Host', cascade='all,delete-orphan')
    relations = relationship('Relation', backref='channel', cascade='all,delete-orphan')

    @validates('name')
    def validateName(self, attr, name):
        '''
        Validate name
        @param attr: name of attr 
        @param name: value of attr
        @return: str
        '''
        validateLength(name, 'channel name', 3, 100)
        return name

    def asDict(self):
        '''
        Return dict with object's data
        @return: dict
        '''
        return {
            'name': self.name,
            'hosts': tuple(h.asDict() for h in self.hosts)
        }


class Host(Base):
    '''
    Class represents host
    '''
    __tablename__ = 'hosts'
    __table_args__ = (
        UniqueConstraint('channelId', 'name', name='unique_host_on_channel'),
        {
            'mysql_engine': 'InnoDB',
            'mysql_charset': 'utf8'
        }
    )

    id = Column(Integer, primary_key=True)
    channelId = Column(Integer, ForeignKey(Channel.id))
    name = Column(String(100, collation='utf8_bin'), nullable=False)
    isSni = Column(Boolean, default=False)

    @validates('name')
    def validateName(self, attr, name):
        '''
        Validate name
        @param attr: name of attr 
        @param name: value of attr
        @return: str
        '''
        validateLength(name, 'hostname', 3, 100)
        return name

    def asDict(self):
        '''
        Return dict with object's data
        @return: dict
        '''
        return {
            'name': self.name,
            'isSni': self.isSni
        }

class Relation(Base):
    '''
    Class for storing relations between ip, region and channel
    '''
    __tablename__ = 'relations'
    __table_args__ = {'mysql_engine': 'InnoDB', 'mysql_charset': 'utf8'}

    id = Column(Integer, primary_key=True)
    default = Column('def', Boolean, nullable=False, default=False)    
    ipId = Column(Integer, ForeignKey('ips.id'), nullable=False)
    regionId = Column(Integer, ForeignKey('regions.id'), nullable=False)
    channelId = Column(Integer, ForeignKey('channels.id'))

    def asDictForIp(self):
        '''
        Return channel: region dict
        @return: dict
        '''
        if self.default:
            return { 'default': self.region.name }
        else:
            return { self.channel.name: self.region.name }