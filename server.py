# -*- coding: utf-8 -*-
'''
thanks to Guyon Morée http://gumuz.looze.net/
'''
import SocketServer
from SimpleXMLRPCServer import SimpleXMLRPCServer

class AsyncXMLRPCServer(SocketServer.ThreadingMixIn,SimpleXMLRPCServer):
    pass