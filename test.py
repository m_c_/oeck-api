# -*- coding: utf-8 -*-
import unittest

class Test0User(unittest.TestCase):
    name = 'testUserName'

    def test_00_add_user_success(self):
        res = ws.addUser(self.name, 'password')
        self.assertEqual(res, 0)

    def test_01_add_user_error(self):
        res = ws.addUser(self.name, '')
        self.assertEqual(res, 1)

    def test_02_set_password_success(self):
        res = ws.setPassword(self.name, 'password1')
        self.assertEqual(res, 0)

    def test_03_set_password_error(self):
        res = ws.setPassword(self.name, '')
        self.assertEqual(res, 1)

    def test_04_check_pass_success(self):
        res = ws.checkPassword(self.name, 'password1')
        self.assertEqual(res, 0)

    def test_05_check_pass_error(self):
        res = ws.checkPassword(self.name, '123456')
        self.assertEqual(res, 2)

    def test_06_get_objects_success(self):
        res = ws.getUserObjects()
        self.assertIsInstance(res, list)

    def test_07_del_user_success(self):
        res = ws.delUser(self.name)
        self.assertEqual(res, 0)

    def test_08_del_user_error(self):
        res = ws.delUser('')
        self.assertEqual(res, 1)


class Test1Service(unittest.TestCase):
    user = 'testUserForServices'

    @classmethod
    def setUpClass(cls):
        ws.addUser(cls.user, 'password')
        super(Test1Service, cls).setUpClass()

    @classmethod
    def tearDownClass(cls):
        ws.delUser(cls.user)
        super(Test1Service, cls).tearDownClass()

    def test_00_set_service_success(self):
        res = ws.setService(self.user, 'VPN')
        self.assertEqual(res, 0)

    def test_01_set_service_error(self):
        res = ws.setService(self.user, 'no service')
        self.assertEqual(res, 1)

    def test_02_get_service_list_success(self):
        res = ws.getServiceList(self.user)
        self.assertIsInstance(res, list)

    def test_03_get_service_list_error(self):
        res = ws.getServiceList('no user')
        self.assertEqual(res, 1)

    def test_04_clear_service_success(self):
        res = ws.clearService(self.user, 'VPN')
        self.assertEqual(res, 0)

    def test_05_clear_service_error(self):
        res = ws.clearService(self.user, 'no service')
        self.assertEqual(res, 1)


class Test2Region(unittest.TestCase):
    name = 'testRegion'

    def test_00_add_region_success(self):
        res = ws.addRegion(self.name, 'server.Host.Name', 'friendlyName')
        self.assertEqual(res, 0)

    def test_01_add_region_error(self):
        res = ws.addRegion('', 'serverHostName', 'friendlyName')
        self.assertEqual(res, 1)

    def test_02_rename_region_success(self):
        res = ws.renameRegion(self.name, 'newFriendlyName')
        self.assertEqual(res, 0)

    def test_03_rename_region_error(self):
        res = ws.renameRegion('', 'newFriendlyName')
        self.assertEqual(res, 1)

    def test_04_change_host_success(self):
        res = ws.changeRegionHost(self.name, 'new.Host')
        self.assertEqual(res, 0)

    def test_05_change_host_error(self):
        res = ws.changeRegionHost('', 'newHost')
        self.assertEqual(res, 1)

    def test_06_add_host_success(self):
        res = ws.addRegionSpecificHost(self.name, 'host.host', '127.0.0.1')
        self.assertEqual(res, 0)

    def test_07_add_host_error(self):
        res = ws.addRegionSpecificHost('', 'host', 'ip')
        self.assertEqual(res, 1)

    def test_08_del_host_success(self):
        res = ws.delRegionSpecificHost(self.name, 'host.host')
        self.assertEqual(res, 0)

    def test_09_del_host_error(self):
        res = ws.delRegionSpecificHost('', 'host')
        self.assertEqual(res, 1)

    def test_10_list_regions_success(self):
        res = ws.listRegions()
        self.assertIsInstance(res, list)

    def test_11_get_objects_success(self):
        res = ws.getRegionObjects()
        self.assertIsInstance(res, list)

    def test_12_del_region_success(self):
        res = ws.delRegion(self.name)
        self.assertEqual(res, 0)

    def test_13_del_region_error(self):
        res = ws.delRegion('')
        self.assertEqual(res, 1)


class Test3Channel(unittest.TestCase):
    name = 'testChannel'
    region = 'testRegionForChannel'

    @classmethod
    def setUpClass(cls):
        ws.addRegion(cls.region, 'server.Host.Name', 'friendlyName')
        super(Test3Channel, cls).setUpClass()

    @classmethod
    def tearDownClass(cls):
        ws.delRegion(cls.region)
        super(Test3Channel, cls).tearDownClass()

    def test_00_add_channel_success(self):
        res = ws.addChannel(self.name)
        self.assertEqual(res, 0)

    def test_01_add_channel_error(self):
        res = ws.addChannel('')
        self.assertEqual(res, 1)

    def test_02_list_channels_success(self):
        res = ws.listChannels()
        self.assertIsInstance(res, list)

    def test_03_add_channel_record_success(self):
        res = ws.addChannelRecord(self.name,'host',True)
        self.assertEqual(res, 0)

    def test_04_add_channel_record_error(self):
        res = ws.addChannelRecord('','host',True)
        self.assertEqual(res, 1)

    def test_05_list_channel_records_success(self):
        res = ws.listChannelRecords(self.name)
        self.assertIsInstance(res, list)

    def test_06_list_channel_records_error(self):
        res = ws.listChannelRecords('')
        self.assertEqual(res, 1)

    def test_07_del_channel_record_success(self):
        res = ws.delChannelRecord(self.name,'host')
        self.assertEqual(res, 0)

    def test_08_del_channel_record_error(self):
        res = ws.delChannelRecord('','host')
        self.assertEqual(res, 1)

    def test_09_get_objects_success(self):
        res = ws.getChannelObjects()
        self.assertIsInstance(res, list)

    def test_10_lock_channel_success(self):
        res = ws.lockChannel(self.name, self.region)
        self.assertEqual(res, 0)

    def test_11_lock_channel_error(self):
        res = ws.lockChannel('', '')
        self.assertEqual(res, 1)

    def test_12_unlock_channel_success(self):
        res = ws.unlockChannel(self.name)
        self.assertEqual(res, 0)

    def test_13_unlock_channel_error(self):
        res = ws.unlockChannel('')
        self.assertEqual(res, 1)

    def test_14_del_channel_success(self):
        res = ws.delChannel(self.name)
        self.assertEqual(res, 0)

    def test_15_del_channel_error(self):
        res = ws.delChannel('')
        self.assertEqual(res, 1)
   

class Test4Auth(unittest.TestCase):
    user = 'testUserForAuth'
    region1 = 'testRegion1ForAuth'
    region2 = 'testRegion2ForAuth'
    channel = 'testChannelForAuth'

    @classmethod
    def setUpClass(cls):
        ws.addUser(cls.user, 'password')
        ws.addRegion(cls.region1, 'test1.server.com', 'friendlyName1')
        ws.addRegion(cls.region2, 'test2.server.com', 'friendlyName2')
        ws.addChannel(cls.channel)
        super(Test4Auth, cls).setUpClass()

    @classmethod
    def tearDownClass(cls):
        ws.delChannel(cls.channel)
        ws.delRegion(cls.region1)
        ws.delRegion(cls.region2)
        ws.delUser(cls.user)
        super(Test4Auth, cls).tearDownClass()

    def test_00_auth_add_success(self):
        res = ws.authorizeIP('127.0.0.1', self.user, self.region1, {self.channel: self.region2}, 3600)
        self.assertEqual(res, 0)

    def test_01_auth_update_success(self):
        res = ws.authorizeIP('127.0.0.1', self.user, self.region1, {self.channel: self.region2}, 3600)
        self.assertEqual(res, 3)

    def test_02_auth_error(self):
        res = ws.authorizeIP('', self.user, self.region1, {self.channel: self.region2}, 3600)
        self.assertEqual(res, 1)

    def test_03_auth_limit_error(self):
        ws.authorizeIP('127.0.0.2', self.user, self.region1, {self.channel: self.region2}, 3600)
        res = ws.authorizeIP('127.0.0.3', self.user, self.region1, {self.channel: self.region2}, 3600)
        self.assertEqual(res, 2)

    def test_04_deauth_success(self):
        res = ws.deauthorizeIP('127.0.0.1')
        self.assertEqual(res, 0)

    def test_05_deauth_error(self):
        res = ws.deauthorizeIP('')
        self.assertEqual(res, 1)


if __name__ == '__main__':
    from xmlrpclib import ServerProxy
    from settings import DB, TEST_DB, SERVER

    if TEST_DB['db']:
        DB.update(TEST_DB)

    ws = ServerProxy("http://127.0.0.1:%s" % SERVER['port'])
    unittest.main()