# -*- coding: utf-8 -*-
'''
Run this script once for initializing DB
'''
def init():
        '''
        For creating db tables
        '''
        from sqlalchemy.orm import sessionmaker
        from sqlalchemy import create_engine
        from settings import getDbUrl
        from classes import initDb, Service

        session = None
        try:
            Session = sessionmaker()
            engine = create_engine(getDbUrl(),isolation_level="READ COMMITTED") 
            Session.configure(bind=engine)
            session = Session()
            initDb(session)
            session.add_all([
                Service(name='VPN'),
                Service(name='vDNS'),
                Service(name='proxyDNS')
            ])
            session.commit()
        except BaseException as e:
            raise e
        finally:
            if session:
                session.close()

if __name__ == '__main__':
    init()