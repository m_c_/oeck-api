# -*- coding: utf-8 -*-
import logging
from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine, exc, select
from settings import getDbUrl, DEBUG

logging.basicConfig(
    level = logging.DEBUG if DEBUG else logging.ERROR,
    format = '[%(asctime)s - %(levelname)s - %(filename)s:%(lineno)d (%(funcName)s)] %(message)s'
)

class WebService(object):
    '''
    Class provides API
    '''

    session = None
    logger = None

    def __init__(self):
        '''
        Initializing
        '''
        self.logger = logging.getLogger(__name__)

    def __initSession(self):
        '''
        Initializing db, not part of API
        every API method (using DB) must begin with this method
        more on handling DB disconnect: http://docs.sqlalchemy.org/en/rel_1_0/core/pooling.html?highlight=ping
        '''
        if self.session is None:
            engine = create_engine(getDbUrl(),isolation_level="READ COMMITTED") 
            Session = sessionmaker(bind=engine)
            self.session = Session()
        else:
            ''' if server restarted or connection expired '''
            try:
                self.session.execute(select([1]))
            except exc.DBAPIError as err:
                self.session.rollback()
                if err.connection_invalidated:
                    self.session.execute(select([2]))
                else:
                    raise

    def __getObject(self, name, cls):
        '''
        Get object from DB, not part of API
        @param name: str
        @param cls: class
        @return: object 
        '''
        self.__initSession()
        obj = self.session.query(cls).filter_by(name=name).first()
        if obj is None:
            raise Exception('Object \'{0}\' not found'.format(name))
        return obj

    #**********# user functions #**********#

    def checkUser(self,name):
        '''
        Checks if user exists
        @param name: str
        @return: int 0 - not exist, 1 - failure, 2 - exist
        '''
        try:
            from classes import User

            self.__initSession()
            if self.session.query(User).filter_by(name=name).first():
                return 2
            return 0
        except BaseException as e:
            self.logger.error(e)
            self.session.rollback()
            return 1
        finally:
            self.session.close()

    def addUser(self,name,password):
        '''
        Creates a new account on the system and the LDAP backend
        @param name: str
        @param password: str
        @return: int 0 - success, 1 - failure
        '''
        try:
            from classes import User
            from ldap import Ldap

            inldap = False
            self.__initSession()
            user = User(name = name, password = User.securePassword(password))
            ldap = Ldap()
            inldap = ldap.addUser(user)
            self.session.add(user)
            self.session.commit()
            return 0
        except BaseException as e:
            self.logger.error(e)
            self.session.rollback()
            if inldap:
                try:
                    ldap.delUser(user)
                except BaseException as e:
                    self.logger.error(e)
            return 1
        finally:
            self.session.close()

    def delUser(self,name):
        '''
        Delete account referred by the name from the system and LDAP
        @param name: str
        @return: int 0 - success, 1 - failure
        '''
        try:
            from classes import User
            from ldap import Ldap

            inldap = False
            user = self.__getObject(name, User)
            ldap = Ldap()
            inldap = ldap.delUser(user)
            self.session.delete(user)
            self.session.commit()
            return 0
        except BaseException as e:
            self.logger.error(e)
            self.session.rollback()
            if inldap:
                try:
                    ldap.addUser(user)
                except BaseException as e:
                    self.logger.error(e)
            return 1
        finally:
            self.session.close()

    def setPassword(self,name,password):
        '''
        Set a password on the referred name on the LDAP
        @param name: str
        @param password: str
        @return: int 0 - success, 1 - failure
        '''
        try:
            from classes import User
            from ldap import Ldap

            inldap = False
            user = self.__getObject(name, User)
            ldap = Ldap()
            oldPass = user.password
            user.password = User.securePassword(password)
            inldap = ldap.setPassword(user)
            self.session.commit()
            return 0
        except BaseException as e:
            self.logger.error(e)
            self.session.rollback()
            if inldap:
                try:
                    user.password = oldPass
                    ldap.setPassword(user)
                except BaseException as e:
                    self.logger.error(e)
            return 1
        finally:
            self.session.close()

    def checkPassword(self,name,password):
        '''
        Check if a password matches the LDAP server
        @param name: str
        @param password: str
        @return: int 0 - success, 1 - failure, 2 - incorrect credentials
        '''
        try:
            from classes import User
            from ldap import Ldap

            user = self.__getObject(name, User)
            ldap = Ldap()
            user.password = User.securePassword(password)
            return 0 if ldap.checkPassword(user) else 2
        except BaseException as e:
            self.logger.error(e)
            self.session.rollback()
            return 1
        finally:
            self.session.close()

    def resyncToLDAP(self,name):
        '''
        Clear the username from LDAP and recreate it from the database
        @return: int 0 - success, 1 - failure
        '''
        try:
            from classes import User
            from ldap import Ldap

            user = self.__getObject(name, User)
            ldap = Ldap()
            ldap.delUser(user)
            ldap.addUser(user)
            self.session.commit()
            return 0
        except BaseException as e:
            self.logger.error(e)
            self.session.rollback()
            return 1
        finally:
            self.session.close()

    def getUserObjects(self):
        '''
        Return a JSON serialized listing of the User objects
        @return: tuple - success, 1 - failure
        '''
        try:
            from classes import User
            from json import dumps

            self.__initSession()
            return tuple(dumps(user.asDict()) for user in self.session.query(User).all())
        except BaseException as e:
            self.logger.error(e)
            self.session.rollback()
            return 1
        finally:
            self.session.close()

    #**********# ip functions #**********#

    def authorizeIP(self,addr,username,region,exceptions,ttl):
        '''
        Authorize an IP to use the system.
        Put message in the RabbitMQ queue containing the parameters passed to the function and function name
        @param addr: str
        @param username: str
        @param region: str
        @param exceptions: dict {'name of the channel': 'name of the region'}
        @param ttl: int
        @return: int
            0 - success,
            1 - failure,
            2 - allowed number of IP's for user is exceeded,
            3 - ip updated
        '''
        try:
            from time import time
            from classes import User, Ip, Region, Relation, Channel
            from mq import Mq

            user = self.__getObject(username, User)
            region = self.__getObject(region, Region)

            #looking for ip, if not found - create new, else - update existent
            result = 0
            ip = None
            expire = time() + int(ttl)
            for tmp in user.ips:
                if tmp.ip == addr:
                    ip = tmp
                    ip.expire = expire
                    ip.relations[:] = []
                    result = 3
                    break
            if ip is None:
                if user.ipCount == 0:
                    self.logger.error('Allowed number of IP\'s for the user \'{0}\' is exceeded'.format(username))
                    return 2
                ip = Ip(ip=addr, expire=expire)

            #create relations between ip, regions and channels
            with self.session.no_autoflush:
                rel = Relation(default=True)
                rel.region = region
                ip.relations.append(rel)
                for channel in exceptions:
                    rel = Relation(default=False)
                    rel.region = self.__getObject(exceptions[channel], Region)
                    rel.channel = self.__getObject(channel, Channel)
                    ip.relations.append(rel)

            user.ips.append(ip)
            if result == 0:
                user.ipCount -= 1
            self.session.commit()
            Mq().publishFuncCall('authorizeIP', {
                'addr': addr,
                'username': username,
                'region': region,
                'exceptions': ','.join(('{0}={1}'.format(c, exceptions[c]) for c in exceptions)),
                'ttl': ttl
            })
            return result
        except BaseException as e:
            self.logger.error(e)
            self.session.rollback()
            return 1
        finally:
            self.session.close()

    def deauthorizeIP(self,addr):
        '''
        Deauthorize an IP from the system
        Put message in the RabbitMQ queue containing the parameters passed to the function and function name
        @param addr: str
        @return: int 0 - success, 1 - failure
        '''
        try:
            from classes import Ip
            from mq import Mq

            self.__initSession()
            ip = self.session.query(Ip).filter(Ip.ip==addr).first()
            if ip is None:
                raise Exception('IP {0} not found'.format(addr))
            self.session.delete(ip)
            ip.user.ipCount += 1
            self.session.commit()
            Mq().publishFuncCall('deauthorizeIP', {'addr': addr})
            return 0
        except BaseException as e:
            self.logger.error(e)
            self.session.rollback()
            return 1
        finally:
            self.session.close()

    #**********# service functions #**********#

    def setService(self,username,servicename):
        '''
        Enable a service for the user
        Current available services are: proxyDNS,vDNS,VPN
        @param username: str
        @param servicename: str
        @return: int 0 - success, 1 - failure
        '''
        try:
            from classes import User, Service
            from ldap import Ldap

            inldap = False
            user = self.__getObject(username, User)
            service = self.__getObject(servicename, Service)
            ldap = Ldap()
            old = user.services
            user.services.append(service)
            inldap = ldap.setServices(user)
            self.session.commit()
            return 0
        except BaseException as e:
            self.logger.error(e)
            self.session.rollback()
            if inldap:
                try:
                    user.services = old
                    ldap.setServices(user)
                except BaseException as e:
                    self.logger.error(e)
            return 1
        finally:
            self.session.close()

    def clearService(self,username,servicename):
        '''
        Disable a service for the user
        Current available services are: proxyDNS,vDNS,VPN
        @param username: str
        @param servicename: str
        @return: int 0 - success, 1 - failure
        '''
        try:
            from classes import User, Service
            from ldap import Ldap

            user = self.__getObject(username, User)
            service = self.__getObject(servicename, Service)
            ldap = Ldap()
            inldap = False
            old = user.services
            if service in user.services:
                user.services.remove(service)
                inldap = ldap.setServices(user)
            self.session.commit()
            return 0
        except BaseException as e:
            self.logger.error(e)
            self.session.rollback()
            if inldap:
                try:
                    user.services = old
                    ldap.setServices(user)
                except BaseException as e:
                    self.logger.error(e)
            return 1
        finally:
            self.session.close()

    def getServiceList(self,username):
        '''
        Return list of enabled services for User as an array
        @param username: str
        @return: tuple - success, 1 - failure
        '''
        try:
            from classes import User

            user = self.__getObject(username, User)
            return tuple(service.name for service in user.services)
        except BaseException as e:
            self.logger.error(e)
            self.session.rollback()
            return 1
        finally:
            self.session.close()

    #**********# region functions #**********#

    def addRegion(self,name,server,friendlyName):
        '''
        Add a region to the system
        Put message in the RabbitMQ queue containing the parameters passed to the function and function name
        @param name: str internal name used as a key
        @param server: str FQDN of the entry point for customer traffic
        @param friendlyName: str name to be displayed to the customer
        @return: int 0 - success, 1 - failure
        '''
        try:
            from classes import Region
            from mq import Mq

            self.__initSession()
            region = Region(name=name,server=server,friendlyName=friendlyName)
            self.session.add(region)
            self.session.commit()
            Mq().publishFuncCall(
                'addRegion', {'name': name,'server': server,'friendlyName': friendlyName}
            )
            return 0
        except BaseException as e:
            self.logger.error(e)
            self.session.rollback()
            return 1
        finally:
            self.session.close()
    
    def delRegion(self,name):
        '''
        Delete region from the system
        Put message in the RabbitMQ queue containing the parameters passed to the function and function name
        @param name: str
        @return: int 0 - success, 1 - failure
        '''
        try:
            from classes import Region
            from mq import Mq

            region = self.__getObject(name, Region)
            self.session.delete(region)
            self.session.commit()
            Mq().publishFuncCall('delRegion', {'name': name})
            return 0
        except BaseException as e:
            self.logger.error(e)
            self.session.rollback()
            return 1
        finally:
            self.session.close()
    
    def renameRegion(self,name,newName):
        '''
        Change region friendlyName
        @param name: str
        @param newName: str
        @return: int 0 - success, 1 - failure
        '''
        try:
            from classes import Region

            region = self.__getObject(name, Region)
            region.friendlyName = newName
            self.session.commit()
            return 0
        except BaseException as e:
            self.logger.error(e)
            self.session.rollback()
            return 1
        finally:
            self.session.close()
        
    def changeRegionHost(self,name,newServer):
        '''
        Change the FQDN for the referred zone
        @param name: str
        @param newServer: str
        @return: int 0 - success, 1 - failure
        '''
        try:
            from classes import Region

            region = self.__getObject(name, Region)
            region.server = newServer
            self.session.commit()
            return 0
        except BaseException as e:
            self.logger.error(e)
            self.session.rollback()
            return 1
        finally:
            self.session.close()

    def addRegionSpecificHost(self,name,host,addr):
        '''
        Add a non SNI IP address to the region
        @param name: str
        @param hostname: str
        @param addr: str
        @return: int 0 - success, 1 - failure
        '''
        try:
            from classes import Region, SpecHost

            region = self.__getObject(name, Region)
            ip = SpecHost(name=host,ip=addr)
            region.specHosts.append(ip)
            self.session.commit()
            return 0
        except BaseException as e:
            self.logger.error(e)
            self.session.rollback()
            return 1
        finally:
            self.session.close()

    def delRegionSpecificHost(self,name,host):
        '''
        Delete a non SNI Hostname from the region
        @param name: str
        @param host: str
        @return: int 0 - success, 1 - failure
        '''
        try:
            from classes import Region

            region = self.__getObject(name, Region)
            for sh in region.specHosts:
                if sh.name == host:
                    region.specHosts.remove(sh)
            self.session.commit()
            return 0
        except BaseException as e:
            self.logger.error(e)
            self.session.rollback()
            return 1
        finally:
            self.session.close()

    def listRegions(self):
        '''
        Return the list of regions names in the system
        @return: tuple - success, 1 - failure
        '''
        try:
            from classes import Region

            self.__initSession()
            return tuple(r.name for r in self.session.query(Region).all())
        except BaseException as e:
            self.logger.error(e)
            self.session.rollback()
            return 1
        finally:
            self.session.close()

    def getRegionObjects(self):
        '''
        Return a JSON serialized listing of the Region objects
        @return: tuple - success, 1 - failure
        '''
        try:
            from classes import Region
            from json import dumps

            self.__initSession()
            return tuple(dumps(reg.asDict()) for reg in self.session.query(Region).all())
        except BaseException as e:
            self.logger.error(e)
            self.session.rollback()
            return 1
        finally:
            self.session.close()

    #**********# channel functions #**********#

    def addChannel(self,name):
        '''
        Add a channel (Netflix, hulu, etc)
        Put message in the RabbitMQ queue containing the parameters passed to the function and function name
        @param name: str
        @return: int 0 - success, 1 - failure
        ''' 
        try:
            from classes import Channel
            from mq import Mq

            self.__initSession()
            channel = Channel(name=name)
            self.session.add(channel)
            self.session.commit()
            Mq().publishFuncCall('addChannel', {'name': name})
            return 0
        except BaseException as e:
            self.logger.error(e)
            self.session.rollback()
            return 1
        finally:
            self.session.close()

    def delChannel(self,name):
        '''
        Delete a channel from the system and all associated records
        Put message in the RabbitMQ queue containing the parameters passed to the function and function name
        @param name: str
        @return: int 0 - success, 1 - failure
        '''
        try:
            from classes import Channel
            from mq import Mq

            channel = self.__getObject(name, Channel)
            self.session.delete(channel)
            self.session.commit()
            Mq().publishFuncCall('delChannel', {'name': name})
            return 0
        except BaseException as e:
            self.logger.error(e)
            self.session.rollback()
            return 1
        finally:
            self.session.close()

    def addChannelRecord(self,name,host,sni):
        '''
        Add a hostname record to a channel.
        Put message in the RabbitMQ queue containing the parameters passed to the function and function name
        @param name: str
        @param host: str
        @param sni: boolean specify if the hostname is SNI compliant or not
        @return: int 0 - success, 1 - failure
        '''
        try:
            from classes import Channel, Host
            from mq import Mq

            channel = self.__getObject(name, Channel)
            channel.hosts.append(Host(name=host,isSni=True if sni else False))
            self.session.commit()
            Mq().publishFuncCall(
                'addChannelRecord', {'name': name, 'host': host, 'sni': sni}
            )
            return 0
        except BaseException as e:
            self.logger.error(e)
            self.session.rollback()
            return 1
        finally:
            self.session.close()

    def delChannelRecord(self,name,host):
        '''
        Remove channel host record from the system
        Put message in the RabbitMQ queue containing the parameters passed to the function and function name
        @param name: str
        @param host: str
        @return: int 0 - success, 1 - failure
        '''
        try:
            from classes import Channel
            from mq import Mq

            channel = self.__getObject(name, Channel)
            for h in channel.hosts:
                if host == h.name:
                    channel.hosts.remove(h)
            self.session.commit()
            Mq().publishFuncCall(
                'delChannelRecord', {'name': name, 'host': host}
            )
            return 0
        except BaseException as e:
            self.logger.error(e)
            self.session.rollback()
            return 1
        finally:
            self.session.close()
    
    def listChannelRecords(self,name):
        '''
        Return the list of the hostnames associated with the channel
        @param name: str
        @return: tuple - success, 1 - failure
        '''
        try:
            from classes import Channel

            channel = self.__getObject(name, Channel)
            return tuple(h.name for h in channel.hosts)
        except BaseException as e:
            self.logger.error(e)
            self.session.rollback()
            return 1
        finally:
            self.session.close()
        
    def listChannels(self):
        '''
        Return list of channels in the system
        @return: tuple - success, 1 - failure
        '''
        try:
            from classes import Channel

            self.__initSession()
            return tuple(c.name for c in self.session.query(Channel).all())
        except BaseException as e:
            self.logger.error(e)
            self.session.rollback()
            return 1
        finally:
            self.session.close()

    def getChannelObjects(self):
        '''
        Return a JSON serialized listing of the Channel objects
        @return: tuple - success, 1 - failure
        '''
        try:
            from classes import Channel
            from json import dumps

            self.__initSession()
            return tuple(dumps(ch.asDict()) for ch in self.session.query(Channel).all())
        except BaseException as e:
            self.logger.error(e)
            self.session.rollback()
            return 1
        finally:
            self.session.close()

    def lockChannel(self,name,region):
        ''' 
        Lock a specific channel to a region
        @param name: str
        @param region: str
        @return: 0 - success, 1 - failure
        '''
        try:
            raise Exception('Not implemented')
        except BaseException as e:
            self.logger.error(e)
            self.session.rollback()
            return 1
    
    def unlockChannel(self,name):
        ''' 
        Unlock a specific channel
        @param name: str
        @return: 0 - success, 1 - failure
        '''
        try:
            raise Exception('Not implemented')
        except BaseException as e:
            self.logger.error(e)
            self.session.rollback()
            return 1