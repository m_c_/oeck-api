# -*- coding: utf-8 -*-
import pika
from contextlib import contextmanager
from settings import MQ


class Mq(object):
    '''
    Class for working with RabbitMQ server
    '''

    @contextmanager
    def connection(self):
        '''
        Open and close connection to server
        '''
        params = pika.URLParameters(
            'amqp://{0}:{1}@{2}:{3}'.format(
                MQ['user'], MQ['pass'], MQ['host'], MQ['port']
            )
        )
        conn = pika.BlockingConnection(params)
        self.channel = conn.channel()
        yield
        conn.close()

    def publish(self, message):
        '''
        Publish message to queue
        @param message: str
        '''
        with self.connection():
            self.channel.basic_publish(
                '',
                MQ['queue'],
                message,
                pika.BasicProperties(content_type='text/plain',delivery_mode=1)
            )

    def publishFuncCall(self, name, params):
        '''
        Create message from dict and publish it
        @param name: str function name 
        @param data: dict params  
        '''
        if params:
            msg = 'Function \'{0}\' called with params: '.format(name)
            msg = msg + ','.join('{0}={1}'.format(k, params[k]) for k in params)
        else:
            msg = 'Function \'{0}\' called'.format(name)
        self.publish(msg)