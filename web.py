# -*- coding: utf-8 -*-
from flask import Flask, request, redirect, url_for, render_template, flash
from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine
from logging import getLogger
from settings import getDbUrl
from api import WebService
from classes import User , Service, Region, Channel

DEBUG = True
SECRET_KEY = 'not very secret developer\'s key'
logger = getLogger(__name__)
app = Flask(__name__)
app.config.from_object(__name__)

Session = sessionmaker()
engine = create_engine(getDbUrl())
Session.configure(bind=engine)
session = Session()

@app.route('/')
def home_view():
    return redirect(url_for('view_users'))

#**********# user functions #**********#

@app.route('/users/')
def view_users():
    users = session.query(User).order_by(User.id)
    return render_template('users.html', users=users)

@app.route('/users/add', methods=['POST'])
def add_user():
    ws = WebService()
    res = ws.addUser(request.form['username'], request.form['password'])
    if res == 1:
        flash('Error adding user')
    return redirect(url_for('view_users'))

@app.route('/users/del', methods=['POST'])
def del_user():
    ws = WebService()
    res = ws.delUser(request.form['username'])
    if res == 1:
        flash('Error deleting user')
    return redirect(url_for('view_users'))

@app.route('/user/<int:uid>')
def view_user(uid):
    user = session.query(User).get(uid)
    services = session.query(Service).all()
    ws = WebService()
    us = ws.getServiceList(user.name)
    regions = ws.listRegions()
    channels = ws.listChannels()
    return render_template('user.html', user=user,services=services,us=us,regions=regions,channels=channels)

#**********# service functions #**********#

@app.route('/users/add-service', methods=['POST'])
def add_service():
    ws = WebService()
    res = ws.setService(request.form['username'], request.form['service_name'])
    if res == 1:
        flash('Error adding service to user')
    return redirect(url_for('view_user', uid=request.form['uid']))

@app.route('/users/del-service', methods=['POST'])
def del_service():
    ws = WebService()
    res = ws.clearService(request.form['username'], request.form['service_name'])
    if res == 1:
        flash('Error removing service from user')
    return redirect(url_for('view_user', uid=request.form['uid']))

#**********# region functions #**********#

@app.route('/users/add-ip', methods=['POST'])
def add_ip():
    d = {}
    for i, channel in enumerate(request.form.getlist('channels')):
        d[channel] = request.form.getlist('regions')[i]
        
    ws = WebService()
    res = ws.authorizeIP(
        request.form['ip'],
        request.form['username'],
        request.form['region'],
        d,
        request.form['ttl']
    )
    if res == 1:
        flash('Error adding ip to user')
    return redirect(url_for('view_user', uid=request.form['uid']))

@app.route('/users/del-ip', methods=['POST'])
def del_ip():
    ws = WebService()
    res = ws.deauthorizeIP(request.form['ip'])
    if res == 1:
        flash('Error removing ip from user')
    return redirect(url_for('view_user', uid=request.form['uid']))

#**********# region functions #**********#


@app.route('/regions/')
def view_regions():
    ws = WebService()
    regions = [r for r in session.query(Region).all() if r.name in ws.listRegions()]
    return render_template('regions.html', regions=regions)

@app.route('/regions/add', methods=['POST'])
def add_region():
    ws = WebService()
    res = ws.addRegion(request.form['name'],request.form['host'],request.form['friendlyName'])
    if res == 1:
        flash('Error adding region')
    return redirect(url_for('view_regions'))

@app.route('/regions/del', methods=['POST'])
def del_region():
    ws = WebService()
    res = ws.delRegion(request.form['name'])
    if res == 1:
        flash('Error deleting region')
    return redirect(url_for('view_regions'))

@app.route('/region/<int:rid>')
def view_region(rid):
    region = session.query(Region).get(rid)
    return render_template('region.html', region=region)

@app.route('/regions/rename', methods=['POST'])
def rename_region():
    ws = WebService()
    res = ws.renameRegion(request.form['name'], request.form['friendlyName'])
    if res == 1:
        flash('Error renaming region')
    return redirect(url_for('view_region', rid=request.form['rid']))

@app.route('/regions/change-server', methods=['POST'])
def change_server_region():
    ws = WebService()
    res = ws.changeRegionHost(request.form['name'], request.form['server'])
    if res == 1:
        flash('Error changing region server')
    return redirect(url_for('view_region', rid=request.form['rid']))

@app.route('/regions/add-host', methods=['POST'])
def add_host_region():
    ws = WebService()
    res = ws.addRegionSpecificHost(request.form['name'], request.form['host'], request.form['ip'])
    if res == 1:
        flash('Error adding host to region')
    return redirect(url_for('view_region', rid=request.form['rid']))

@app.route('/regions/del-host', methods=['POST'])
def del_host_region():
    ws = WebService()
    res = ws.delRegionSpecificHost(request.form['name'], request.form['host'])
    if res == 1:
        flash('Error deleting region')
    return redirect(url_for('view_region', rid=request.form['rid']))

#**********# channel functions #**********#

@app.route('/channels/')
def view_channels():
    ws = WebService()
    channels = [r for r in session.query(Channel).all() if r.name in ws.listChannels()]
    return render_template('channels.html', channels=channels)

@app.route('/channels/add', methods=['POST'])
def add_channel():
    ws = WebService()
    res = ws.addChannel(request.form['name'])
    if res == 1:
        flash('Error adding channel')
    return redirect(url_for('view_channels'))

@app.route('/channels/del', methods=['POST'])
def del_channel():
    ws = WebService()
    res = ws.delChannel(request.form['name'])
    if res == 1:
        flash('Error deleting channel')
    return redirect(url_for('view_channels'))

@app.route('/channel/<int:cid>')
def view_channel(cid):
    channel = session.query(Channel).get(cid)
    ws = WebService()
    hosts = ws.listChannelRecords(channel.name)
    return render_template('channel.html', channel=channel, hosts=hosts)

@app.route('/channels/add-host', methods=['POST'])
def add_host_channel():
    ws = WebService()
    sni = True if 'sni' in request.form else False
    res = ws.addChannelRecord(request.form['name'], request.form['host'], sni)
    if res == 1:
        flash('Error adding host to channel')
    return redirect(url_for('view_channel', cid=request.form['cid']))

@app.route('/channels/del-host', methods=['POST'])
def del_host_channel():
    ws = WebService()
    res = ws.delChannelRecord(request.form['name'], request.form['host'])
    if res == 1:
        flash('Error deleting host from channel')
    return redirect(url_for('view_channel', cid=request.form['cid']))


if __name__ == '__main__':
    app.run()