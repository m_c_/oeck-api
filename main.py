# -*- coding: utf-8 -*-
from logging import error 
from threading import Thread
from sys import exit
from time import sleep, time
from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine
from server import AsyncXMLRPCServer
from api import WebService
from settings import SERVER, getDbUrl
from classes import Ip

def cleanIPs():
    '''
    Check ttls of ip addresses and remove if needed
    @return: int 0 - success, 1 - failure
    '''
    try:
        engine = create_engine(getDbUrl(),isolation_level="READ COMMITTED") 
        Session = sessionmaker(bind=engine)
        session = Session()

        for ip in session.query(Ip).filter(Ip.expire<=time()):
            session.delete(ip)
            ip.user.ipCount += 1
        session.commit()
        return 0
    except BaseException as e:
        error(e)
        session.rollback()
        return 1
    finally:
        session.close()

def main():
    '''
    '''
    wserver = AsyncXMLRPCServer(('', SERVER['port']),logRequests=False)
    wserver.register_instance(WebService())
    wserver.register_multicall_functions()
    wserver.register_introspection_functions()

    t = Thread(target=wserver.serve_forever)
    t.start()

    try:
        while True:
            #place for other functions
            cleanIPs()
            sleep(SERVER['wait'])
    except KeyboardInterrupt:
        wserver.server_close()
        t.join()
        exit(0)

if __name__ == '__main__':
    main()