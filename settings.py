# -*- coding: utf-8 -*-
DEBUG = False
DB = {
    'host': 'localhost',
    'port': 3306,
    'db'  : 'test',
    'user': 'root',
    'pass': 'test123'
}
TEST_DB = {
    'host': None,
    'port': 3306,
    'db'  : None,
    'user': None,
    'pass': None
}
LDAP = {
    'host': None,
    'dc'  : None,#for working with users, like 'ou=Users,dc=domain,dc=com'
    'user': None,#full, like 'cn=admin,dc=domain,dc=com'
    'pass': None
}
MQ = {
    'host' : None,
    'port' : 5672,
    'queue': None,
    'user' : None,
    'pass' : None
}
SERVER = {
    'port': 8080,
    'wait': 5 #seconds
}

def getDbUrl():
    '''
    Format connection string
    @return str 
    '''
    return 'mysql+pymysql://{0}:{1}@{2}:{3}/{4}'.format(DB['user'], DB['pass'], DB['host'], DB['port'], DB['db'])

import os

if (os.path.exists(os.path.join(os.path.dirname(__file__), 'settings_local.py'))):
    from settings_local import *
