# -*- coding: utf-8 -*-
import ldap3
from contextlib import contextmanager
from settings import LDAP

class LdapError(Exception):
    pass

class Ldap(object):
    '''
    Class for working with LDAP server
    '''

    @contextmanager
    def connection(self):
        '''
        Open and close connection to server
        '''
        self.conn = ldap3.Connection(
            LDAP['host'], LDAP['user'], LDAP['pass'], auto_bind=True
        )
        yield
        self.conn.unbind()

    def _getDn(self, username):
        '''
        Format full dn
        @param username: str
        @return: str
        '''
        return 'uid={0},{1}'.format(username, LDAP['dc'])

    def addUser(self, user):
        '''
        Add user
        @param user: User object
        @raise exception: LdapError
        @return: bool success 
        '''
        with self.connection():
            res = self.conn.add(
                self._getDn(user.name),
                ['account','simpleSecurityObject'],
                {'uid': user.name, 'userPassword': user.password, 'services': ''}
            )
            if not res:
                raise LdapError('Error add user \'{0}\' to LDAP: {1}'.format(user.name, self.conn.result))
            return res

    def delUser(self, user):
        '''
        Delete user
        @param user: User object
        @raise exception: LdapError
        @return: bool success 
        '''
        with self.connection():
            res = self.conn.delete(self._getDn(user.name))
            if not res:
                raise LdapError('Error del user \'{0}\' from LDAP: {1}'.format(user.name, self.conn.result))
            return res

    def setPassword(self, user):
        '''
        Change user's password
        @param user: User object
        @return: bool success 
        '''
        with self.connection():
            res = self.conn.modify(
                self._getDn(user.name),
                {'userPassword': (ldap3.MODIFY_REPLACE, user.password)}
            )
            if not res:
                raise LdapError('Error changing password for user \'{0}\': {1}'.format(user.name, self.conn.result))
            return res

    def checkPassword(self, user):
        '''
        Check user's password
        @param user: User object
        @return: bool success 
        '''
        with self.connection():
            return self.conn.compare(self._getDn(user.name), 'userPassword', user.password)

    def setServices(self, user):
        '''
        Set user's services
        @param user: User object
        @return: bool success 
        '''
        with self.connection():
            res = self.conn.modify(
                self._getDn(user.name),
                {'services': (ldap3.MODIFY_REPLACE, ','.join(s.name for s in user.services))}
            )
            if not res:
                raise LdapError('Error setting services for user \'{0}\': {1}'.format(user.name, self.conn.result))
            return res